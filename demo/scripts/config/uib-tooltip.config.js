﻿(function () {
    'use strict';

    angular.module("funcef-demo")
        .config(['$uibTooltipProvider', function ($uibTooltipProvider) {
            $uibTooltipProvider.options({
                appendToBody: true
            });
        }]);
})();