﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Loading
    * @version 1.0.0
    * @Componente Loading da pagina
    */

    angular.module('funcef-loading.directive', []);

    angular
    .module('funcef-loading', [
      'funcef-loading.directive'
    ]);
})();