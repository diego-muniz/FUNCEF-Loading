﻿(function () {
    'use strict';

    angular
      .module('funcef-loading.directive')
      .directive('ngfLoading', ngfLoading);

    ngfLoading.$inject = [];

    /* @ngInject */
    function ngfLoading() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/loading.view.html',
            controllerAs: 'vm',
            scope: {
                'principal': '=',
            }
        };
    }
})();