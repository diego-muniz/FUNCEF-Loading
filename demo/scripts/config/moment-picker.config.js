﻿(function () {
    'use strict';

    angular.module('funcef-demo')
        .config(['momentPickerProvider', function (momentPickerProvider) {
            momentPickerProvider.options({
                locale: "pt-br",
                format: "L",
                today: true,
                startView: 'day',
                keyboard: false
            });
        }]);
})();