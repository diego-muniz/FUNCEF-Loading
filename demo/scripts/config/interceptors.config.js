﻿(function () {
    'use strict';

    angular.module("funcef-demo")
        .config(['$httpProvider', '$locationProvider', function ($httpProvider, $locationProvider, $filter) {
            $httpProvider.defaults.withCredentials = true;

            $httpProvider.interceptors.push(function ($q, $rootScope) {
                return {
                    'request': function (config) {
                        $rootScope.requesting++;
                        return config || $q.when(config);
                    },
                    'requestError': function (rejection) {
                        $rootScope.requesting--;
                        return $q.reject(response);
                    },
                    'response': function (response) {
                        $rootScope.requesting--;
                        return response || $q.when(response);
                    },
                    'responseError': function (rejection) {
                        $rootScope.requesting--;
                        return $q.reject(rejection)
                    }
                };
            });

            // ISO 8601 Date Pattern: YYYY-mm-ddThh:MM:ss
            var dateMatchPattern = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/;
            var decimalMatchPattern = /^-?(([1-9]\d*)|0)(.0*[1-9](0*[1-9])*)?$/;

            var convertDates = function (obj) {
                for (var key in obj) {
                    if (!obj.hasOwnProperty(key)) continue;

                    var value = obj[key];
                    var typeofValue = typeof (value);

                    if (typeofValue === 'object') {
                        // If it is an object, check within the object for dates.
                        convertDates(value);
                    } else if (typeofValue === 'string') {
                        if (dateMatchPattern.test(value)) {
                            obj[key] = new Date(value);
                        }
                    }
                }
            }

            $httpProvider.defaults.transformResponse.push(function (data) {
                if (typeof (data) === 'object') {
                    convertDates(data);
                }

                return data;
            });
        }]);
})();

