# Componente - FUNCEF-Loading

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#modulo)
6. [Uso](#uso)
7. [Parâmetros](#parâmetros)
8. [Desinstalação](#desinstalação)


## Descrição

- Componente Loading gera uma imagem para o momento de carregamento.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-loading --save.
```

## Script

```html
<script src="bower_components/funcef-loading/dist/funcef-loading.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-loading/dist/funcef-loading.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-file dentro do módulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-loading']);
```

## Uso

```html
<ngf-loading></ngf-loading>
```

### Parâmetros:

- ngf-loading (Obrigatório);

## Desinstalação:

```
bower uninstall funcef-loading --save
```