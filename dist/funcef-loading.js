(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Loading
    * @version 1.0.0
    * @Componente Loading da pagina
    */

    angular.module('funcef-loading.directive', []);

    angular
    .module('funcef-loading', [
      'funcef-loading.directive'
    ]);
})();;(function () {
    'use strict';

    angular
      .module('funcef-loading.directive')
      .directive('ngfLoading', ngfLoading);

    ngfLoading.$inject = [];

    /* @ngInject */
    function ngfLoading() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/loading.view.html',
            controllerAs: 'vm',
            scope: {
                'principal': '=',
            }
        };
    }
})();;angular.module('funcef-loading').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/loading.view.html',
    "<div ng-show=\"$root.requesting\" class=\"cssload-container\" ng-class=\"{ 'hidden': !$root.requesting || !$root.usuario || $root.loaderDisabled}\"> <div class=\"cssload-speeding-wheel\"></div> </div>"
  );

}]);
