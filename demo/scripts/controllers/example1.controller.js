﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = ['$rootScope'];

    /* @ngInject */
    function Example1Controller($rootScope) {
        var vm = this;

        vm.ativar = ativar;
        vm.desativar = desativar;

        init();

        ///////

        function init() {
        	desativar();
        }

     	function ativar() {
     		$rootScope.requesting = 1;
     		$rootScope.usuario = {'nome' : 'Usuário 1'};
     	}   

     	function desativar() {
     		$rootScope.requesting = 0;
     		$rootScope.usuario = {};
     	}   
    }
}());